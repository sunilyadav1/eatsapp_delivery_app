package delivery.grazzy.app;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText input_name, input_ph;
    private TextInputLayout input_layout_name, input_layout_ph;
    Button generate;
    LinearLayout parent_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.getInstance().set_task_bg(this);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = getWindow();
//
//            // clear FLAG_TRANSLUCENT_STATUS flag:
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//
//// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//
//// finally change the color
//            window.setStatusBarColor(Color.parseColor(getString(R.string.my_statusbar_color)));
//        }


        setContentView(R.layout.activity_main);

        if (AppController.getInstance().sharedPreferences.getBoolean("login", false)) {
            Intent i = new Intent(MainActivity.this,Home.class);
            startActivity(i);
            finish();
        }else {
            getdid();
        }

        parent_layout=(LinearLayout)findViewById(R.id.parent_layout);

        input_layout_name = (TextInputLayout) findViewById(R.id.input_layout_name);
        input_layout_ph = (TextInputLayout) findViewById(R.id.input_layout_ph);

        input_name = (EditText) findViewById(R.id.input_name);
        input_ph = (EditText) findViewById(R.id.input_ph);

        generate = (Button) findViewById(R.id.generate);

        input_name.addTextChangedListener(new MyTextWatcher(input_name));
        input_ph.addTextChangedListener(new MyTextWatcher(input_ph));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        input_name.clearFocus();


        Toast.makeText(this,"Welcome",Toast.LENGTH_LONG).show();

        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateName() && validatePh()) {

                    AppController.getInstance().sharedPreferences_editor.putString("firstname", input_name.getText().toString());
                    AppController.getInstance().sharedPreferences_editor.putString("phone", input_ph.getText().toString());
                    AppController.getInstance().sharedPreferences_editor.commit();

                    if(!ConnectivityReceiver.isConnected())
                    {
                        Snackbar snackbar = Snackbar
                                .make(parent_layout, "No internet connection!", Snackbar.LENGTH_LONG);
                        build_no_internet_msg();

                        snackbar.show();

                        build_no_internet_msg();

                    }else
                    {
                        startActivity(new Intent(MainActivity.this, Verification.class));
                        finish();
                    }

//                    ask_permission();


                } else {

                    Log.e("validateName() " + validateName(), " validatePh() " + validatePh());

                }

            }
        });


    }

    private void getdid() {

        if (FirebaseInstanceId.getInstance().getToken() != null) {

            Log.e("old token : ", "" + FirebaseInstanceId.getInstance().getToken());
            AppController.getInstance().token=true;
            AppController.getInstance().token_value=FirebaseInstanceId.getInstance().getToken();

        } else {

            Log.e("fetching : ", "new did");
            FirebaseInstanceId.getInstance().getToken();

        }


    }

    private boolean validateName() {
        if (input_name.getText().toString().length() == 0) {
            input_layout_name.setError("Enter your name");
            requestFocus(input_name);
            return false;
        } else {
            input_layout_name.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validatePh() {

        if (input_ph.getText().toString().length() < 10 ) {
            input_layout_ph.setError("Enter your mobile number");
            requestFocus(input_ph);
            return false;

        } else {

            input_layout_ph.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


//    private void ask_permission() {
//
//        Dexter.withActivity(MainActivity.this)
//                .withPermissions(
//                        android.Manifest.permission.READ_SMS,
//                        android.Manifest.permission.RECEIVE_SMS
//                ).withListener(new MultiplePermissionsListener() {
//
//            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
//
//                if(report.areAllPermissionsGranted())
//                {
//                    if(!ConnectivityReceiver.isConnected())
//                    {
//                        Snackbar snackbar = Snackbar
//                                .make(parent_layout, "No internet connection!", Snackbar.LENGTH_LONG);
//                        build_no_internet_msg();
//
//                        snackbar.show();
//
//                        build_no_internet_msg();
//
//                    }else
//                    {
//                        startActivity(new Intent(MainActivity.this, Verification.class));
//                        finish();
//                    }
//
//                }else if(report.isAnyPermissionPermanentlyDenied())
//                {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                    builder.setTitle("Permission");
//                    builder.setMessage("Delivery app needs SMS permissions to authenticate the user");
//                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//
//                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                            Uri uri = Uri.fromParts("package", getPackageName(), null);
//                            intent.setData(uri);
//                            startActivityForResult(intent,1);
//                            Toast.makeText(getBaseContext(), "Go to Permissions to Grant SMS", Toast.LENGTH_LONG).show();
//                        }
//                    });
//                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    });
//                    builder.show();
//
//                }else
//                {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                    builder.setTitle("Permission");
//                    builder.setMessage("Delivery app needs SMS permissions to authenticate the user");
//                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                            ask_permission();
//                        }
//                    });
//                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    });
//                    builder.show();
//
//                }
//
//                Log.e("report","areAllPermissionsGranted"+report.areAllPermissionsGranted()+"");
//                Log.e("report","getDeniedPermissionResponses"+report.getDeniedPermissionResponses()+"");
//                Log.e("report","getGrantedPermissionResponses"+report.getGrantedPermissionResponses()+"");
//                Log.e("report","isAnyPermissionPermanentlyDenied"+report.isAnyPermissionPermanentlyDenied()+"");
//            }
//
//            @Override
//            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//
//                token.continuePermissionRequest();
//
//            }
//
//
//        }).check();
//    }

    public void build_no_internet_msg() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }






    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName();
                    break;
                case R.id.input_ph:
                    validatePh();
                    break;

            }
        }

    }

}
